import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ErrorBoundary from "./components/sharedComponents/Error/ErrorBoundary";
import {Provider} from "react-redux";
import store from "./store/store";

ReactDOM.render(
    <ErrorBoundary>
        <React.StrictMode>
            <Provider store={store}>
                <App/>
            </Provider>
        </React.StrictMode>
    </ErrorBoundary>,
    document.getElementById('root')
);


/*
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from "react-redux";
import store from './store'

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
  document.getElementById('root')
);

 */
