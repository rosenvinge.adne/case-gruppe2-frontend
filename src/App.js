import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import ROUTE_PATH from "./util/paths/RoutePath";

import Landing from "./components/Landing/Landing";
import CompanyLogin from "./components/CompanyLogin/CompanyLogin";
import CompanyProfile from "./components/CompanyProfile/CompanyProfile";
import CompanyRegister from "./components/CompanyRegister/CompanyRegister";
import CaseHandlerLogin from "./components/Login/CaseHandlerLogin/CaseHandlerLogin";
import {CaseHandlerRouter} from "./components/CaseHandler/CaseHandlerRouter";
import Home from "./components/CaseHandler/CaseHandlerHome/Home";
import CaseHandlerCompanyProfile from "./components/CaseHandler/CompanyProfile/CaseHandlerCompanyProfile";
import ReviewApplication from "./components/CaseHandler/ReviewApplication/ReviewApplication";
import ApplicationReceipt from "./components/CompanyRegister/ApplicationReceipt";
import CompanyRoute from "./util/Routes/CompanyRoute";
import CaseHandlerRegister from "./components/Login/CaseHandlerLogin/CaseHandlerRegister";
import TwoFactor from "./components/Login/TwoFactor/TwoFactor";
import ApplicationRegister from "./components/CompanyProfile/ApplicationRegister";
import ContactRegister from "./components/CompanyProfile/ContactRegister";
import MissingContent from "./components/sharedComponents/Error/MissingContent";

function App() {
    return (
        <div>

            <BrowserRouter>

                <Switch>
                    <Route exact path={ROUTE_PATH.LANDING} component={Landing}/>
                    <Route path={ROUTE_PATH.COMPANY_LOGIN} component={CompanyLogin}/>
                    <CompanyRoute path={ROUTE_PATH.COMPANY_PROFILE} component={CompanyProfile}/>

                    <CompanyRoute path={ROUTE_PATH.COMPANY_ADD_APPLICATION} component={ApplicationRegister} />
                    <CompanyRoute path={ROUTE_PATH.COMPANY_ADD_CONTACT} component={ContactRegister} />

                    <Route path={ROUTE_PATH.COMPANY_REGISTRATION} component={CompanyRegister}/>
                    <Route path={ROUTE_PATH.COMPANY_APPLICATION_RECEIPT} component={ApplicationReceipt}/>

                    <Route path={ROUTE_PATH.CH_LOGIN} component={CaseHandlerLogin} />
                    <Route path={ROUTE_PATH.CH_REGISTER} component={CaseHandlerRegister} />
                    <Route path={ROUTE_PATH.TWO_FACTOR} component={TwoFactor}/>

                    <CaseHandlerRouter exact path={ROUTE_PATH.CH_HOME} component={Home}/>
                    <CaseHandlerRouter exact path={ROUTE_PATH.CH_CUSTOMER_PROFILE + '/:orgnumber'} component={CaseHandlerCompanyProfile}/>
                    <CaseHandlerRouter exact path={ROUTE_PATH.CH_REVIEW_APPLICATION + '/:applicationid'} component={ReviewApplication}/>

                    <Route path={'*'} component={MissingContent}/>

                </Switch>
            </BrowserRouter>


        </div>
    );
}

export default App;
