import {useDispatch, useSelector} from "react-redux";
import {getCompanyInfoFromBRREG} from "../CompanyRegister/CompanyRegisterAPI";
import {companyInfoApplicationRegisterAction} from "../../store/actions/companyInfoActions";
import {storageGet} from "../../util/storage/storage";
import {STORAGE_KEYS} from "../../util/storage/storageKeys";
import {Button} from "@material-ui/core";
import {useHistory} from "react-router-dom";
import ROUTE_PATH from "../../util/paths/RoutePath";
import {useEffect, useState} from "react";
import {getCompanyInfoFromEnhetsReg} from "../../util/apiRequests/apiRequestEnhetsReg";
import styles from "./../CompanyRegister/CompanyRegister.module.css"

function ApplicationRegister() {

    const history = useHistory()

    const id = storageGet(STORAGE_KEYS.COMPANY).username

    const {bankrupt, underLiquidation, companyName, address} = getCompanyInfoFromBRREG(id)
    const dispatch = useDispatch()

    const [companyInfo, setCompanyInfo] = useState(null)

    useEffect(() => {
        if (companyInfo === null) {
            getCompanyInfoFromEnhetsReg(id)
                .then(response => {
                        setCompanyInfo(response)
                })
        }
    }, [])


    // Post application
    async function applicationRegister() {
        dispatch(companyInfoApplicationRegisterAction({id, bankrupt, underLiquidation}))
    }

    // onclick -> Apply for application button
    async function onApplyClick() {

        console.log(id, bankrupt, underLiquidation, companyName, address)
        await applicationRegister();
        history.push(ROUTE_PATH.COMPANY_PROFILE)
    }

    // Onclick -> go home
    const goHomeOnClick = () => {
        history.push(ROUTE_PATH.COMPANY_PROFILE)
    }

    return (
        <div>

            <nav>
                <form className="form-inline">
                    <h3>Aprila</h3>
                    <button onClick={goHomeOnClick}>Profil</button>
                </form>
            </nav>

            <div className={styles.registrationForm}>
                <div className={styles.blueSection}>

                </div>

               <div className={styles.formContainer} style={{textAlign: 'left'}}>
                   {companyInfo &&
                   <div>
                       <h2>Informasjon hentet fra Enhetsregisteret</h2>

                       <p className={styles.applicationText}>Navn: {companyInfo.navn}</p>
                       <p className={styles.applicationText}>Organisasjonsnummer: {companyInfo.organisasjonsform.beskrivelse}</p>

                       <p className={styles.applicationText}>Konkurs: {!bankrupt && <small className={styles.applicationText}>Nei</small>} {bankrupt && <p className={styles.applicationText}>Ja</p>}</p>
                       <p className={styles.applicationText}>Under avvikling: {!underLiquidation && <small className={styles.applicationText}>Nei</small>} {underLiquidation && <p className={styles.applicationText}>Ja</p>}</p>
                   </div>
                   }


                   <button className={styles.button} onClick={onApplyClick}>SØK</button>
               </div>
            </div>

        </div>
    )

}

export default ApplicationRegister