import {useDispatch} from "react-redux";
import {useForm} from "react-hook-form";
import {useState} from "react";
import {companyInfoContactRegisterAction} from "../../store/actions/companyInfoActions";
import {storageGet} from "../../util/storage/storage";
import {STORAGE_KEYS} from "../../util/storage/storageKeys";
import {Button} from "@material-ui/core";
import {Link, useHistory} from "react-router-dom";
import ROUTE_PATH from "../../util/paths/RoutePath";
import {PATTERN} from "../../util/inputValidation/patterns";
import styles from "../CompanyRegister/CompanyRegister.module.css";


function ContactRegister() {
    const id = storageGet(STORAGE_KEYS.COMPANY).username

    const dispatch = useDispatch()
    const history = useHistory()
    const [error, setError] = useState('')
    const {register, handleSubmit, errors} = useForm();
    const [contact, setContact] = useState({
        fullName: '',
        personNumber: '',
        address: '',
        phone: '',
        email: ''
    })

    const onInputChange = event => {
        setContact({
            ...contact,
            [event.target.name]: event.target.value
        })
    }

    // Post contact
    async function contactRegister() {
        dispatch(companyInfoContactRegisterAction({...contact, company: {id: id}}))
    }

    async function onSubmit() {
        try {
            await contactRegister()
        } catch (e) {
            console.log(e.message)
            setError(e.message)
        }
        alert('Kontakt registrert 😊')
    }

    const goHomeOnClick = () => {
        history.push(ROUTE_PATH.COMPANY_PROFILE)
    }

    return (
        <div>
            <nav>
                <form className="form-inline">
                    <h3>Aprila</h3>
                    <button onClick={goHomeOnClick}>Profil</button>
                </form>
            </nav>

            <div className={styles.registrationForm}>
                <div className={styles.blueSection}></div>

                <div className={styles.formContainer}>
                    <h2>REGISTRER KONTAKT</h2>

                    {/* CONTACT REGISTER FORM */}
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <input className={styles.inputField} placeholder="Fullt navn" name="fullName"
                               onChange={onInputChange}
                               ref={register({required: true, pattern: PATTERN.NAME})}/>
                        {errors.fullName && errors.fullName.type === 'required' &&
                        <p className={styles.inputError}>Obligatorisk felt</p>}
                        {errors.fullName && errors.fullName.type === 'pattern' &&
                        <p className={styles.inputError}>Kun bokstaver</p>}
                        {!errors.fullName && <p style={{height: '1em'}}></p>}


                        <input className={styles.inputField} placeholder="Personnummer" name="personNumber"
                               onChange={onInputChange} ref={register({
                            required: true,
                            minLength: 11,
                            maxLength: 11,
                            pattern: PATTERN.NUMBER
                        })}/>
                        {errors.personNumber && errors.personNumber.type === 'required' &&
                        <p className={styles.inputError}>Obligatorisk felt</p>}
                        {errors.personNumber && errors.personNumber.type === 'pattern' &&
                        <p className={styles.inputError}>Kun tall</p>}
                        {errors.personNumber && (errors.personNumber.type === 'minLength' || errors.personNumber.type === 'maxLength') &&
                        <p className={styles.inputError}>Personnummer består av 11 siffer</p>}
                        {!errors.personNumber && <p style={{height: '1em'}}></p>}


                        <input className={styles.inputField} placeholder="Adresse" name="address"
                               onChange={onInputChange}
                               ref={register({required: true, pattern: PATTERN.ADDRESS})}/>
                        {errors.address && errors.address.type === 'required' &&
                        <p className={styles.inputError}>Obligatorisk felt</p>}
                        {errors.address && errors.address.type === 'pattern' &&
                        <p className={styles.inputError}>Ugyldige tegn</p>}
                        {!errors.address && <p style={{height: '1em'}}></p>}

                        <input className={styles.inputField} placeholder="Telefon" name="phone" onChange={onInputChange}
                               ref={register({required: true, pattern: PATTERN.NUMBER})}/>
                        {errors.phone && errors.phone.type === 'required' &&
                        <p className={styles.inputError}>Obligatorisk felt</p>}
                        {errors.phone && errors.phone.type === 'pattern' &&
                        <p className={styles.inputError}>Kun tall</p>}
                        {!errors.phone && <p style={{height: '1em'}}></p>}

                        <input className={styles.inputField} placeholder="E-post" name="email" onChange={onInputChange}
                               ref={register({required: true, pattern: PATTERN.EMAIL})}/>
                        {errors.email && errors.email.type === 'required' &&
                        <p className={styles.inputError}>Obligatorisk felt</p>}
                        {errors.email && errors.email.type === 'pattern' &&
                        <p className={styles.inputError}>Ugyldig e-postadresse</p>}
                        {!errors.email && <p style={{height: '1em'}}></p>}

                        <input className={styles.button} type="submit" value="REGISTRER" ref={register}/>
                    </form>

                    {/* IF AN ERROR OCCURS WHEN POSTING CONTACT TO DB */}
                    {error && <p>error</p>}

                </div>
            </div>
        </div>
    )

}

export default ContactRegister