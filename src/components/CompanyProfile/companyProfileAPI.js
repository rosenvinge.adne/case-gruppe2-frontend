import {API_PATHS} from "../../util/apiPaths/apiPaths";
import {storageGet} from "../../util/storage/storage";
import {STORAGE_KEYS} from "../../util/storage/storageKeys";


// Fetch access token from local storage
const updateCustomerFromLocalStorageKey = () => {
    return storageGet(STORAGE_KEYS.USER)
}


export function hasPendingApplication () {
    const customer = updateCustomerFromLocalStorageKey()

    const getHeader = {
        method: 'GET',
        headers:{
            Accept: 'application/json',
            Authorization: `Bearer ${customer.accessToken}`
        }
    }

    return fetch(`${API_PATHS.BASE}/api/v1/company/company/${customer.username}/pending`, getHeader)
        .then(response => response.json())

}