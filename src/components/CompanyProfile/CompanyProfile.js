import LoaderComponent from "../sharedComponents/Loader/Loader";
import CompanyInformationDisplay from "../sharedComponents/InformationDisplay/CompanyInformationDisplay";
import {API_PATHS} from "../../util/apiPaths/apiPaths";
import {storageGet, storageRemove, storageSet} from "../../util/storage/storage";
import {STORAGE_KEYS} from "../../util/storage/storageKeys";
import {Link, useHistory} from "react-router-dom";
import ROUTE_PATH from "../../util/paths/RoutePath";
import {Button} from "@material-ui/core";
import {useEffect, useState} from "react";
import {hasPendingApplication} from "./companyProfileAPI";
import 'bootstrap/dist/css/bootstrap.css';
import styles from "./Navbar.module.css"


function CompanyProfile() {

    //const companyId = useSelector(state => state.companyProfileReducer.companyProfile.username)

    const history = useHistory()
    const company = storageGet(STORAGE_KEYS.COMPANY)

    const [hasPendingApp, setHasPendingApp] = useState(false)

    let fetchedPending = false

    useEffect(() =>{
        if(!fetchedPending){
            fetchedPending = true
            hasPendingApplication()
                .then(response => {
                    setHasPendingApp(response)
                })
                .catch(e => console.log('error' + e.message))
        }
    }, [fetchedPending])


    function onClickLogout(){
        storageRemove(STORAGE_KEYS.COMPANY)
        history.push(ROUTE_PATH.LANDING)
    }

    const addContactOnClick = () => {
        history.push(ROUTE_PATH.COMPANY_ADD_CONTACT)
    }

    const addApplicationOnClick = () => {
        history.push(ROUTE_PATH.COMPANY_ADD_APPLICATION)
    }

    return (
        <div>
            <nav>
                <form className="form-inline">
                    <h3>Aprila</h3>
                    <button onClick={addContactOnClick}>Legg til kontakt</button>
                    {!hasPendingApp &&
                    <button onClick={addApplicationOnClick}>Ny søknad</button>}
                    <button className={styles.logoutBtn} onClick={onClickLogout}><span style={{color: 'white'}} className="material-icons">logout</span></button>
                </form>
            </nav>
            <div style={{margin: '1em 10% 0 10%'}}>
                <h2>Kundeprofil</h2>

                {company &&
                <CompanyInformationDisplay companyEndpoint={`${API_PATHS.GET_CUSTOMER_PATH}/${company.username}`}/>
                }

                {!company &&
                <LoaderComponent/>
                }

            </div>
        </div>
    )
}

export default CompanyProfile
