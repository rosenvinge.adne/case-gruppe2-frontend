import {Card, CardContent, TextField} from "@material-ui/core";
import styles from "./../../CompanyLogin/CompanyLogin.module.css";
import 'bootstrap/dist/css/bootstrap.css';

function CaseHandlerLoginForm({handleChange}){


    return(
        <div>
            <div className={styles.inputFieldContainer}>
                <span className="material-icons">perm_identity</span>
                <input className={styles.inputField}
                    id="username"
                    placeholder="Brukernavn"
                    onChange={handleChange}/>
            </div>

            <div><p style={{height: '1em'}}></p></div>

            <div className={styles.inputFieldContainer}>
                <span className="material-icons">lock</span>
                <input className={styles.inputField}
                       id="password"
                       placeholder="Passord"
                       type="password"
                       onChange={handleChange}/>
            </div>
        </div>
    )
}

export default CaseHandlerLoginForm