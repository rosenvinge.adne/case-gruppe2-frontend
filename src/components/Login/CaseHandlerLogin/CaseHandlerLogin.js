import ROUTE_PATH from "../../../util/paths/RoutePath";
import {Link} from "react-router-dom";
import {Button, Card, CardContent} from "@material-ui/core";
import Alert from '@material-ui/lab/Alert';

import CaseHandlerLoginForm from "./CaseHandlerLoginForm";
import {useEffect, useState} from "react";
import {loginCaseHandler} from "../../../util/apiRequests/apiRequests";
import {STORAGE_KEYS} from "../../../util/storage/storageKeys";
import {storageSet} from "../../../util/storage/storage";
import "./CaseHandlerLogin.css"
import TwoFactor from "../TwoFactor/TwoFactor";
import styles from "./../../CompanyLogin/CompanyLogin.module.css";
import 'bootstrap/dist/css/bootstrap.css';

export function CaseHandlerLogin() {

    const [state, setState] = useState({
            username: '',
            password: ''
        }
    )
    // Response from initial api request. We care about: response.message
    const [response, setResponse] = useState({})

    const [error, setError] = useState('')

    // Set username and password onchange
    const onChange = (e) => {
        const {id, value} = e.target
        setState(prevState => ({
            ...prevState,
            [id]: value.toLowerCase()
        }))
        setError('')
    }

    useEffect(() =>{
        if (response){
            if(response.message === 'Verify user to log in!'){
                storageSet(STORAGE_KEYS.USER, response)
            }
        }
    }, [response])

    const loginOnclick = e => {
        if (state.username === '' || state.password === ''){
            setError('Feil brukernavn eller passord')
        }else{
            loginCaseHandler(state) // api call
                .then(r => {
                    setResponse(r)
                    console.log('Response:' + response.toString())

                    if(r.status === 401){ // catch error -> 'Invalid login'
                        setError('Feil brukernavn eller passord')
                    }
                    if(r.message === 'Too many failed attempts.'){
                        setError('Kontoen er sperret')
                    }
                })
        }
    }

    return (
        <div>
            <nav>
                <form className="form-inline">
                    <Link to={ROUTE_PATH.LANDING}>
                        <button type="button"><h3>Aprila</h3></button>
                    </Link>
                </form>
            </nav>

            <div className={styles.card}>
                <div className={styles.leftContainer}>

                </div>
                <div className={styles.formContainer}>
                    <div>
                        {(response.message !== 'Verify user to log in!') &&
                        <div>
                            <h2>LOGG INN</h2>
                            <h6>SAKSBEHANDLER</h6>

                            <CaseHandlerLoginForm handleChange={onChange}/>

                            <br/>
                            <button className={styles.button} type="button" disabled={error}
                                    onClick={loginOnclick}
                                    variant="contained"
                                    color="primary">LOGG INN</button>
                            <br/>
                            {!error && <div className={styles.noErrorBox}></div>}

                            {error && <Alert severity="error">{error}</Alert>}

                            { /* <p>Don't have an account? <Link to={ROUTE_PATH.CH_REGISTER}>register</Link> here!</p> */ }
                        </div>
                        }

                        {response.message === 'Verify user to log in!' &&
                        <TwoFactor username={state.username} password={state.password}/>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CaseHandlerLogin
