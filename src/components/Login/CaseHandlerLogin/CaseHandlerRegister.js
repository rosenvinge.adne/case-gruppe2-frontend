import {Button, Card, CardContent, TextField} from "@material-ui/core";
import {useState} from "react";
import {registerNewCaseHandler} from "../../../util/apiRequests/apiRequests";
import ROUTE_PATH from "../../../util/paths/RoutePath";
import {Link} from "react-router-dom";
import TwoFactor from "../TwoFactor/TwoFactor";
import {Alert} from "@material-ui/lab";

function CaseHandlerRegister() {

    const [registerInfo, setRegisterInfo] = useState({
        username: '',
        password: ''
    })

    // Response from initial api call. we care about: response.message, response.secretImageUri
    const [response, setResponse] = useState({
        message: ''
    })

    const [error, setError] = useState('')

    const handleChange = (e) => {
        const {id, value} = e.target
        setRegisterInfo(prevState => ({
            ...prevState,
            [id]: value
        }))
        setError('')
    }

    const registerOnClick = () => {
        if (registerInfo.username === '' || registerInfo.password === ''){
            setError('Username or/and password is empty' )
        }else{
            registerNewCaseHandler(registerInfo)
                .then(res => {
                    setResponse(res)
                    if (res.message==='Error: Username is already taken!'){
                        setError(res.message)
                    }
                })

        }

    }


    return (
        <Card>
            <CardContent>
                <h2>Register</h2>

                <TextField
                    id="username"
                    label="Username"
                    onChange={handleChange}
                />
                <br/>

                <TextField
                    id="password"
                    label="Password"
                    type="password"
                    onChange={handleChange}
                />
                <br/>
                <Button onClick={registerOnClick} variant="contained" color="primary">Register</Button>
                {error.length > 0 && <Alert severity="error">{error}</Alert>}
                <p>Go back to <Link to={ROUTE_PATH.CH_LOGIN}>LOGIN</Link></p>
                <p>{response.message}</p>
                {response.message === "User registered successfully!" &&
                <div>
                    <img src={response.secretImageUri} alt={"two step authentication QR code"}/>
                    <TwoFactor username={registerInfo.username} password={registerInfo.password}/>
                </div>
                }

            </CardContent>
        </Card>
    )
}

export default CaseHandlerRegister