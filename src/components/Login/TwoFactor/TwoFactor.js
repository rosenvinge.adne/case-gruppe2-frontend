import {storageGet, storageSet} from "../../../util/storage/storage";
import {STORAGE_KEYS} from "../../../util/storage/storageKeys";
import {useEffect, useState} from "react";
import {Button, TextField} from "@material-ui/core";
import {verifyTwoFactor} from "../../../util/apiRequests/apiRequests";
import ROUTE_PATH from "../../../util/paths/RoutePath";
import {useHistory} from "react-router-dom";
import {Alert} from "@material-ui/lab";
import {useDispatch} from "react-redux";
import {companyProfileSetAction} from "../../../store/actions/companyProfileActions";
import styles from "../../CompanyLogin/CompanyLogin.module.css";



function TwoFactor ({username, password}){

    const [user, setUser] = useState(storageGet(STORAGE_KEYS.USER))
    const [code, setCode] = useState('')
    const dispatch = useDispatch()
    const [error, setError] = useState('')

    function handleChange(e) {
        setCode(e.target.value)
        setError('')
    }


    function onSubmit() {
        // validate
        code.replace(/\s/g, '')

        if (code.length !== 6){
            setError('Verifseringskoden er 6 siffer')
        }else{
            verifyTwoFactor(username,password,code)
                .then(response => {
                    setUser(response)
                    if (response.message === 'Code is incorrect!'){
                        setError('Ugyldig kode')
                    }
                })
        }
    }

    const history = useHistory()
    useEffect(() =>{
        if(user.id > 0){
            storageSet(STORAGE_KEYS.USER, user)
            if (user.roles.includes('ROLE_CASEHANDLER')){
                history.push(ROUTE_PATH.CH_HOME)
            }else if (user.roles.includes('ROLE_CUSTOMER')){
                dispatch(companyProfileSetAction(user));
                history.push(ROUTE_PATH.COMPANY_PROFILE)
            }
        }
    }, [user])

    return (
        <div>
            <h2>Tofaktorautentisering</h2>
            <p>Vennligst fyll inn verifiseringskode</p>
            <TextField
                id="code"
                label="Engangskode"
                onChange={handleChange}
                required
                inputProps={{maxLength : 6}}
            /> <br/> <br/>
            <button className={styles.button} type="button" disabled={error} variant="contained" onClick={onSubmit}>SEND INN</button>
            <br/><br/>
            {error && <Alert severity="error">{error}</Alert>}
        </div>
    )
}

export default TwoFactor
