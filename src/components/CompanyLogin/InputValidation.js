export function validateCompanyNrLogin(input) {
    // Only allows 9 digit numbers
    // 8 wtf??
    return input.trim() !== '' && (/[0-9]/).test(input) && input.length === 8;
}

export function validateCompanyNr(input) {
    // Only allows 9 digit numbers
    return input.trim() !== '' && (/[0-9]/).test(input) && input.length === 9;
}

export function validateInput(id, input) {

    if(id === 'fullName') {
        // only letters, dot, dash and space
        return input.trim() !== '' && (/^[a-zA-ZÆæØøÅå\-.\s]+$/).test(input);
    }
    if(id === 'personNumber'){
        // only 11 digit numbers
        return input.trim() !== '' && (/[0-9]/).test(input) && input.length === 11;
    }
    if(id === 'email'){
        // only email
        return input.trim() !== '' && (/^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/).test(input);
    }
    if(id === 'phone'){
        // only numbers
        return input.trim() !== '' && (/[0-9]/).test(input);
    }
}