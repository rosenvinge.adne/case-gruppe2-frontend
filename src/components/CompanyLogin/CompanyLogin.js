import CompanyLoginForm from "./CompanyLoginForm";
import styles from "./CompanyLogin.module.css";
import {Link} from "react-router-dom";
import ROUTE_PATH from "../../util/paths/RoutePath";

function CompanyLogin() {

    return (
        <div>
            <nav>
                <form className="form-inline">
                    <Link to={ROUTE_PATH.LANDING}>
                        <button type="button"><h3>Aprila</h3></button>
                    </Link>
                </form>
            </nav>

            <div className={styles.card}>
                <div className={styles.leftContainer}>
                </div>
                <div className={styles.formContainer}>
                    <CompanyLoginForm/>
                </div>
            </div>
        </div>
    )
}

export default CompanyLogin
