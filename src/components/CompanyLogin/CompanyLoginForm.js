import {useState} from "react";
import {companySignIn} from "../CompanyRegister/CompanyRegisterAPI";
import TwoFactor from "../Login/TwoFactor/TwoFactor";
import {useForm} from "react-hook-form";
import {PATTERN} from "../../util/inputValidation/patterns";
import styles from "./CompanyLogin.module.css";
import 'bootstrap/dist/css/bootstrap.css';
import {Alert} from "@material-ui/lab";

function CompanyLoginForm() {

    const [companyNr, setCompanyNr] = useState('');
    const [caseNr, setCaseNr] = useState('');
    const [loginResponse, setLoginResponse] = useState({})
    const {register, handleSubmit, errors} = useForm();
    const [error, setError] = useState('')

    // Save username input value
    const onOrgNumberInputChange = event => {
        setCompanyNr(event.target.value)
    }

    // Save password input value
    const onPasswordInputChange = event => {
        setCaseNr(event.target.value)
    }

    // Fetch request with response when login clicked, checking if input is correct
    const onLoginClick = async () => {
        await companySignIn(companyNr, caseNr)
            .then(response => {
                setLoginResponse(response)
                setError('')
                if (response.status === 401) {
                    setError('Feil brukernavn eller passord')
                }
                if (response.message === 'Too many failed attempts.') {
                    setError('Kontoen er sperret')
                }
            })
    }

    return (
        <div>
            {loginResponse.message !== 'Verify user to log in!' &&
            <div>
                <h2>LOGG INN</h2>
                <h6>KUNDE</h6>

                <form onSubmit={handleSubmit(onLoginClick)}>
                    <div className={styles.inputFieldContainer}>
                        <span className="material-icons">person</span>
                        <input className={styles.inputField} name="id" onChange={onOrgNumberInputChange}
                               placeholder="Organisasjonsnummer" ref={register({
                            required: true,
                            minLength: 9,
                            maxLength: 9,
                            pattern: PATTERN.NUMBER
                        })}/>
                    </div>

                    <div>
                        {!errors.id && <p style={{height: '1em'}}></p>}
                        {errors.id && errors.id.type === 'pattern' &&
                        <p style={{height: '1em', color: "darkred"}}>Kun tall er gyldig</p>}
                        {errors.id && (errors.id.type === 'minLength' || errors.id.type === 'maxLength') &&
                        <p style={{height: '1em', color: "darkred"}}>Organisasjonsnummer består av 9 siffer</p>}
                    </div>


                    <div className={styles.inputFieldContainer}>
                        <span className="material-icons">lock</span>
                        <input className={styles.inputField} name="password" type="password" onChange={onPasswordInputChange}
                               placeholder="Passord" ref={register({required: true})}/>
                        {/*errors.password && errors.password.type === 'required'*/}
                    </div>
                    <br/>
                    <input className={styles.button} type="submit" value="LOGG INN" ref={register}/>
                    <br/>
                    {!errors.id && !errors.password && !error && <div className={styles.noErrorBox}></div>}

                    {/* ERROR IF EMPTY FIELDS */}
                    {!error && ((errors.id && errors.id.type === 'required') || (errors.password && errors.password.type === 'required')) &&
                    <Alert severity="error">Fyll inn alle felter</Alert>}

                    {/* ERROR IF LOGIN INFO IS INCORRECT */}
                    {error && (!(errors.id && errors.id.type === 'required') || !(errors.password && errors.password.type === 'required')) &&
                    <Alert severity="error">{error}</Alert>}

                </form>
            </div>}


            {loginResponse.message === 'Verify user to log in!' &&
            <TwoFactor username={companyNr} password={caseNr}/>
            }
        </div>
    )
}

export default CompanyLoginForm