import {useEffect, useState} from "react";
import {getCompanyInfoFromEnhetsReg} from "../../../util/apiRequests/apiRequestEnhetsReg";
import {Card, CardContent} from "@material-ui/core";
import styles from "./CompanyProfileEnhetsRegInfo.module.css"


function CompanyProfileEnhetsRegInfo({orgNumber}) {

    const [companyInfo, setCompanyInfo] = useState(null)

    const [error, setError] = useState({
            message: '',
            status: 0
        })

    useEffect(() => {
        if (companyInfo === null) {
            getCompanyInfoFromEnhetsReg(orgNumber)
                .then(response => {
                    if (response.status !== undefined) {
                        setError({message: 'This is not a company that exists in Enhetsregisteret', status: response.status})
                    } else {
                        setCompanyInfo(response)
                        setError(null)
                    }
                })

        }
    }, [])

    return (
        <div>
            <div className={styles.infoContainer}>
                {error === null &&
                <div className={styles.companyInfoContainer}>
                    <h3>Info From Enhetsregisteret:</h3>
                    {companyInfo &&
                    <div>
                        <h4>Antall ansatte: <strong>{companyInfo.antallAnsatte}</strong></h4>
                        <h4>Beskrivelse: <strong>{companyInfo.naeringskode1.beskrivelse}</strong></h4>
                        <h4>Organisasjonsform: <strong>{companyInfo.organisasjonsform.beskrivelse}</strong></h4>
                    </div>
                    }
                    {!companyInfo &&
                    <p>Nothing to view from the Enhetsregister</p>
                    }
                </div>
                }
                {!error === null &&
                <p>{error.message}</p>
                }

            </div>
        </div>
    )
}

export default CompanyProfileEnhetsRegInfo