import {useParams} from "react-router-dom";

import CompanyInformationDisplay from "../../sharedComponents/InformationDisplay/CompanyInformationDisplay";
import {API_PATHS} from "../../../util/apiPaths/apiPaths";
import CompanyProfileEnhetsRegInfo from "./CompanyProfileEnhetsRegInfo";

/**
 * Display information about a given company by organization number -> The component translates orgnumber to
 * company endpoint to use the shared component
 */
function CaseHandlerCompanyProfile() {

    // Gets the orgnumber from the url
    const {orgnumber} = useParams()

    return (
        <div style={{paddingLeft: '1.5em', paddingTop: '1em', color: '#646566', margin: '1em 10% 0 15%'}}>
            <h1>Kundeinformasjon</h1>
            {orgnumber &&
            <div>
                <CompanyInformationDisplay companyEndpoint={API_PATHS.GET_CUSTOMER_PATH + '/' + orgnumber}
                                           displayPrivateComments={true}/>
                <CompanyProfileEnhetsRegInfo orgNumber={orgnumber}/>
            </div>
            }
        </div>
    )
}

export default CaseHandlerCompanyProfile
