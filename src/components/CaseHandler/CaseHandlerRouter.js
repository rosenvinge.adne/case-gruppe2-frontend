import {Link, Route} from "react-router-dom";

import Navbar from "./Navbar";
import {storageGet} from "../../util/storage/storage";
import {STORAGE_KEYS} from "../../util/storage/storageKeys";
import {Card, CardContent} from "@material-ui/core";
import ROUTE_PATH from "../../util/paths/RoutePath";


/**
 * HOC -> all private (only caseHandler) routes go through this component
 **/
export const CaseHandlerRouter = props => {

    const user = storageGet(STORAGE_KEYS.USER)

    if(user){
        if(user.roles.includes('ROLE_CASEHANDLER')){
            return (
                <div>
                    <Navbar/>
                    <Route {...props}/>
                </div>
            )
        }
    }

    return (
        <div>
            <Card>
                <CardContent>
                    <p><Link to={ROUTE_PATH.LANDING}>You don't have access to this site click here to go back</Link></p>
                </CardContent>
            </Card>
        </div>
    )
}

