import PendingApplications from "./PendingApplications";
import CustomerOverview from "./CustomerOverview";
import {Box, Card, CardContent} from "@material-ui/core";
import styles from "./CaseHandlerHomePage.module.css"


function Home() {
    // TODO: All applications assigned to me?
    return (
        <div style={{marginTop: '2em'}}>
            <div>
                <div className={styles.listElementContainer}>
                    <div className={styles.listColumn}>
                        <h2>Kundeoversikt</h2>
                        <CustomerOverview/>
                    </div>
                    <div className={styles.listColumn}>
                        <h2>Aktive søknader</h2>
                        <PendingApplications/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home