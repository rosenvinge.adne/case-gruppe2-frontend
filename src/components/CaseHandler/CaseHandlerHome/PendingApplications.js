/**
 * Iterates through the pending applications
 * */
import {useEffect, useState} from "react";
import {getAllPendingApplications} from "../../../util/apiRequests/apiRequests";
import {Link} from "react-router-dom";
import ROUTE_PATH from "../../../util/paths/RoutePath";
import {List, ListItem, ListSubheader} from "@material-ui/core";
import styles from "./CaseHandlerHomePage.module.css";
import 'bootstrap/dist/css/bootstrap.css';


/**
 * Display all pending applications in a nice little box
 * */
function PendingApplications() {

    const [pendingApplications, setPendingApplications] = useState([])

    // Onload -> fetch all pending applications
    useEffect(() => {
        getAllPendingApplications()
            .then(applications => {
                console.log(applications)
                setPendingApplications(applications)
            })
    }, [])

    return (
        <div>
            {pendingApplications.length > 0 &&
            <div className="summery-container">
                <List className={styles.listContainer}>
                    {pendingApplications.map((value, index) => (
                        <div key={`${value.id}-${index}`} className={styles.listElement}>

                            <Link to={`${ROUTE_PATH.CH_REVIEW_APPLICATION}/${value.id}`} className={styles.link}
                                  key={index}>
                                <h2 className={styles.headerH2} style={{color: "#3674ad"}}>AKTIV</h2>
                                <h6 className={styles.headerH6}>Organisasjonsnummer: {value.company.split('/').filter(Boolean).pop()}</h6>
                                <h6 className={styles.headerH6}>Kommentarer: {value.comments.length}</h6>
                            </Link>
                        </div>
                    ))}
                </List>
            </div>

            }{pendingApplications.length < 1 &&
        <p>Nothing to view</p>
        }
        </div>
    )
}

export default PendingApplications