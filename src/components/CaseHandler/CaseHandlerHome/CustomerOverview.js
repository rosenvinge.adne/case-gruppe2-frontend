import {useState, useEffect} from "react";
import {getAllCompanies} from "../../../util/apiRequests/apiRequests";
import {List, ListItem, ListSubheader} from "@material-ui/core";
import ROUTE_PATH from "../../../util/paths/RoutePath";
import {Link} from "react-router-dom";
import styles from "./CaseHandlerHomePage.module.css";
import 'bootstrap/dist/css/bootstrap.css';



function CustomerOverview() {

    const [company, setCompany] = useState([])

    // onload -> fetch all companies
    useEffect(() => {
        getAllCompanies()
            .then(comp => {
                setCompany(comp)
            })
    }, [])

    return (
        <div>
            {company.length > 0 &&
            <div>
                <div className="summery-container">
                    <List className={styles.listContainer}>
                        {company.map((value, index) => (
                            <div key={`${value.id}-${index}`} >
                                {value.customer &&
                                <div className={styles.listElement}>
                                    <Link to={`${ROUTE_PATH.CH_CUSTOMER_PROFILE}/${value.id}`} className={styles.link}>
                                        <h2 className={styles.headerH2} style={{color: "#3674ad"}}>{value.name}</h2>
                                        <h6 className={styles.headerH6}>Organisasjonsnummer: {value.id}</h6>
                                    </Link>
                                </div>
                                }
                            </div>
                        ))}
                    </List>
                </div>

            </div>


            }{company.length < 1 &&
        <div>
            <p>Nothing to view</p>
        </div>
        }
        </div>
    )
}
export default CustomerOverview