import ROUTE_PATH from "../../util/paths/RoutePath";
import {useHistory} from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';

import {storageRemove} from "../../util/storage/storage";
import {STORAGE_KEYS} from "../../util/storage/storageKeys";
import styles from "../CompanyProfile/Navbar.module.css";


/***
 * Case handler Navigation bar - lets you navigate to 'home' and search for a spesific organization number.
 * @returns {JSX.Element}
 *
 */
function Navbar() {

    const classes = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    }));
    const history = useHistory()

    const logOutOnClick = () => {
        storageRemove(STORAGE_KEYS.USER)
        history.push(ROUTE_PATH.LANDING)
    }

    const goHomeButtonClick = () => {
        history.push(ROUTE_PATH.CH_HOME)
    }


    return (

        <nav>
            <form className="form-inline">
                <h3>Aprila</h3>
                <button onClick={goHomeButtonClick} className={classes.menuButton}>Hjem</button>
                <button onClick={logOutOnClick} className={classes.menuButton}><span style={{color: 'white'}} className="material-icons">logout</span></button>
            </form>
        </nav>

    )
}

export default Navbar
