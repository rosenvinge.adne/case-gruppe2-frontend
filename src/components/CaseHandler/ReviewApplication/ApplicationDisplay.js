import CompanyInformationDisplay from "../../sharedComponents/InformationDisplay/CompanyInformationDisplay";
import SingleApplicationInformationDisplay
    from "../../sharedComponents/InformationDisplay/SingleApplicationInformationDisplay";

/**
 * This component mostly gived some styling to the two subcomponents
 *
 *
 * REDUNDANT
 * */
function ApplicationDisplay({companyEndpoint, applicationId}) {
    return (
        <div className="application-display-box">
            <h2>Company Information:</h2>
            <SingleApplicationInformationDisplay applicationId={applicationId} />
            <CompanyInformationDisplay companyEndpoint={companyEndpoint} />
        </div>
    )
}

export default ApplicationDisplay
