import ReviewApplicationInput from "./ReviewApplicationInput";

import {useState, useEffect} from "react";
import {
    getSingleApplication,
    postCommentToApplication,
    putApplicationStatus
} from "../../../util/apiRequests/apiRequests";
import {useParams} from "react-router-dom";
import LoaderComponent from "../../sharedComponents/Loader/Loader";
import SingleApplicationInformationDisplay
    from "../../sharedComponents/InformationDisplay/SingleApplicationInformationDisplay";
import styles from "./ReviewApplication.module.css"
import 'bootstrap/dist/css/bootstrap.css';


/*
TODO: Oppdatere kommentarene på siden automatisk når nye postes -> ved å sende ned en boolean til child? -> kanskje fiksa?
*/

function ReviewApplication() {

    const [publicComments, setPublicComments] = useState("")
    const [privateComments, setPrivateComments] = useState("")

    // Get id from application
    const {applicationid} = useParams()

    const [application, setApplication] = useState(null)


    // on load fetch application info
    useEffect(() => {
        getSingleApplication(applicationid)
            .then(application => setApplication(application))
            .catch(e => console.error(e.message))
    }, [applicationid])

    // handles change in the input fields -> via child component
    const onChangePublic = e => {
        setPublicComments((publicComments) => e.target.value)
    }
    const onChangePrivate = e => {
        setPrivateComments((privateComments) => e.target.value)
    }

    // saves the comments to the database
    const saveComments = () => {
        const commentPrivate = privateComments.trim()
        if (commentPrivate.length > 0) {
            postCommentToApplication(applicationid, commentPrivate, false)
                .then(comment => {
                    setApplication({...application, comments: [...application.comments, comment]})
                })
            setPrivateComments('')
        }
        const commentPublic = publicComments.trim()
        if (commentPublic.length > 0) {
            postCommentToApplication(applicationid, commentPublic, true)
                .then(comment => {
                    setApplication({...application, comments: [...application.comments, comment]})
                })
            setPublicComments('')
        }

    }

    // Handlers for the accept and deny button
    const acceptApplicationClick = () => {
        saveComments()
        putApplicationStatus(applicationid, 'true')
            .then(r => console.log(r + ' Application accepted...'))
        setApplication({...application, status: "APPROVED"})

    }
    const denyApplicationClick = () => {
        saveComments()
        putApplicationStatus(applicationid, 'false')
            .then(r => console.log(r + ' Application denied...'))
        setApplication({...application, status: "DENIED"})
        console.log(application)

    }

    return (
        <div>
            {application &&
            <div>
                <div className={styles.applicationContainer}>
                    <div className={styles.row}>

                        <div className="col-md-1">

                        </div>

                        <div className="col-md-7">
                            <SingleApplicationInformationDisplay {...application}/>
                        </div>

                        <div className="col-md-4" id={styles.statusContainer}>
                            {application.status === 'APPROVED' && <h4 style={{color: '#239643'}}>GODKJENT</h4>}
                            {application.status === 'DENIED' && <h4 style={{color: '#E33A3A'}}>AVVIST</h4>}
                            {application.status === 'PENDING' && <h4 style={{color: '#3674ad'}}>AKTIV</h4>}

                            {application.status === 'PENDING' && <button className={styles.acceptButton} type="button" onClick={acceptApplicationClick}>GODKJENN</button>}
                            {application.status === 'PENDING' && <button className={styles.denyButton} type="button" onClick={denyApplicationClick}>AVVIS</button>}

                        </div>

                    </div>

                    <div>
                        {/* Comment box on right bottom */}
                        <h4 className={styles.headerH4}>Legg til kommentar</h4>
                        <ReviewApplicationInput isPrivate={false} handleChange={onChangePublic}/>
                        <ReviewApplicationInput isPrivate={true} handleChange={onChangePrivate}/>
                        <button className={styles.saveButton} onClick={saveComments}>LAGRE</button>
                    </div>

                </div>
            </div>
            }
            {!application &&
            <LoaderComponent/>
            }
        </div>
    )
}

export default ReviewApplication
