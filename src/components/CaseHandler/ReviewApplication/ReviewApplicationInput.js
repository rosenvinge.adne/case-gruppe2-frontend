import styles from './ReviewApplication.module.css'


/**
 *
 * @input: isPrivate: boolean -> is this a private comment
 * @input: handleChange -> when called -> stores the param in parent
 * */
function ReviewApplicationInput({isPrivate, handleChange}) {
    return (
        <div className={styles.commentInputContainer}>
            {isPrivate &&
            <input className={styles.commentFieldPrivate} placeholder="Intern kommentar"
                       defaultValue={""}
                       onChange={handleChange}/>

            }
            {!isPrivate &&
            <input className={styles.commentFieldPublic} placeholder="Kommentar til selskapet"
                       defaultValue={""}
                       onChange={handleChange}/>
            }

        </div>
    )
}

export default ReviewApplicationInput
