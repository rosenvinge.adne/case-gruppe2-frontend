import {Link} from "react-router-dom";
import ROUTE_PATH from "../../util/paths/RoutePath";
import styles from "./Landing.module.css"

function Landing() {

    return (
        <div className={styles.page}>

            <div className={styles.container}>
                <h1>Aprila Bank</h1>

                <div>
                    <Link to={ROUTE_PATH.COMPANY_REGISTRATION}>
                        <button className={styles.whiteButton}>REGISTRER</button>
                    </Link>

                    <Link to={ROUTE_PATH.COMPANY_LOGIN}>
                        <button className={styles.whiteButton}>LOGG INN</button>
                    </Link>
                </div>

                <Link to={ROUTE_PATH.CH_LOGIN}>
                    <button className={styles.blueButton}>LOGG INN KUNDEBEHANDLER</button>
                </Link>
            </div>

        </div>
    )
}

export default Landing
