import {useEffect, useState} from "react";
import CommentDisplay from "./CommentDisplay/CommentDisplay";
import {getAllCommentsInApplication} from "../../../util/apiRequests/apiRequests";
import CompanyInformationDisplay from "./CompanyInformationDisplay";
import styles from "./CommentDisplay/CommentDisplay.module.css"

/**
 * @param: applicationId
 * // TODO: bruke allApplication display
 * */
// display one single application with comments

// {id, status, comments, bankrupt, underLiquidation, caseHandler} =
function SingleApplicationInformationDisplay({id, status, comments, bankrupt, underLiquidation, caseHandler, company}) {



    const [allComments, setAllComments] = useState([])

    useEffect(() => {
        if (id) {
            getAllCommentsInApplication(id)
                .then(c => setAllComments(c))
                .catch(e => console.error(e.message))
        }
    }, [comments, status])


    return (
        <div>
            <div style={{padding: '3em 3em 0 3em'}}>
                <h2>Søknad om å bli kunde</h2>
                <CompanyInformationDisplay companyEndpoint={company}/>
                {id &&
                <div>
                    <p className={styles.applicationText}>Konkurs: {!bankrupt && <small className={styles.applicationText}>Nei</small>} {bankrupt && <p className={styles.applicationText}>Ja</p>}</p>
                    <p className={styles.applicationText}>Under avvikling: {!underLiquidation && <small className={styles.applicationText}>Nei</small>} {underLiquidation && <p className={styles.applicationText}>Ja</p>}</p>
                </div>}
            </div>

            {allComments &&
            <div className={styles.commentContainer}>
                {allComments.map((value) => (
                    <div>
                        <CommentDisplay message={value.message} visible={value.visible} key={value.id}/>
                    </div>
                ))}
            </div>}
        </div>
    )
}

export default SingleApplicationInformationDisplay
