import {useState, useEffect} from "react";
import {getAllApplicationInfo} from "../../../../util/apiRequests/apiRequests";
import {useHistory, useParams} from "react-router-dom";
import CommentDisplay from "../CommentDisplay/CommentDisplay";
import ROUTE_PATH from "../../../../util/paths/RoutePath";
import {Button} from "@material-ui/core";
import styles from "./AllApplicationDisplay.module.css"

/**
 * Displays All information as presented when this application is not in the main focus. It will appare as a dropdown button
 * */

function AllApplicationDisplay({applicationEndpoint, displayPrivateComments}) {

    const [visible, setVisible] = useState(false)

    const [application, setApplication] = useState(null)

    // This is so that when we are on the ReviewApplication page, the application will not display twice
    const {applicationid} = useParams()

    const history = useHistory()


    useEffect(() => {

        if (applicationid !== applicationEndpoint.split('/').filter(Boolean).pop()) {
            if (application === null) {
                getAllApplicationInfo(applicationEndpoint.split('/').filter(Boolean).pop())
                    .then(c => setApplication(c))
                    .catch(e => console.log(e.message))
            }
        }
    }, [applicationEndpoint])

    const visibleOnclick = () => {
        setVisible(!visible)
    }

    const goToApplicationPageClick = () => {
        history.push(`${ROUTE_PATH.CH_REVIEW_APPLICATION}/${application.id}`)
    }

    return (
        <div>
            {application &&
            <div>
                {application.status === 'PENDING' &&
                <button className={styles.pendingApplicationBtn} onClick={visibleOnclick}>Søknad om å bli kunde - AKTIV</button>
                }

                {application.status === 'APPROVED' &&
                <button className={styles.approvedApplicationBtn} onClick={visibleOnclick}>Søknad om å bli kunde - GODKJENT</button>
                }

                {application.status === 'DENIED' &&
                <button className={styles.deniedApplicationBtn} onClick={visibleOnclick}>Søknad om å bli kunde - AVVIST</button>
                }

                {visible &&
                <div>
                    {displayPrivateComments &&
                    <button className={styles.applicationPageBtn} onClick={goToApplicationPageClick}>Gå til søknad</button>
                    }
                    <p className={styles.applicationText}>Konkurs: {!application.bankrupt && <small className={styles.applicationText}>Nei</small>} {application.bankrupt && <p className={styles.applicationText}>Ja</p>}</p>
                    <p className={styles.applicationText}>Under avvikling: {!application.underLiquidation && <small className={styles.applicationText}>Nei</small>} {application.underLiquidation && <p className={styles.applicationText}>Ja</p>}</p>

                    {application.comments.length > 0 &&
                    <div style={{width: '40%'}}>

                        {application.comments.map((value) => (
                            <div key={value.id}>
                                {(!displayPrivateComments && value.visible) &&
                                <CommentDisplay message={value.message} visible={value.visible}/>
                                }{displayPrivateComments &&
                            <CommentDisplay message={value.message} visible={value.visible}/>
                            }
                            </div>
                        ))}
                    </div>
                    }
                </div>
                }

            </div>

            }
        </div>
    )
}

export default AllApplicationDisplay
