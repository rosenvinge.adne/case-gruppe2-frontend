import {useEffect} from "react";
import {getFromEndpoint} from "../../../../util/apiRequests/apiRequests";
import {useState} from "react";
import LoaderComponent from "../../Loader/Loader";

import "./ContactsDisplay.css"
import {Card, CardContent} from "@material-ui/core";
import styles from './ContactDisplay.module.css'
import 'bootstrap/dist/css/bootstrap.css';

// endre navn til ContactDisplay
function ContactsDisplay({contactsEndpoint}) {

    const [contact, setContact] = useState(null)

    useEffect(() => {
        if (contact === null) {
            getFromEndpoint(contactsEndpoint)
                .then(c => setContact(c))
                .catch(e => console.log(e.message))
        }
    }, [contactsEndpoint])

    return (
        <Card className={styles.contactDisplayBox} style={{width: 'auto'}}>
            <CardContent>
                {contact &&
                <div>
                    {contact.primaryContact &&
                    <p className={styles.contactHeaderBox} >HOVEDKONTAKTPERSON</p>
                    }
                    {!contact.primaryContact &&
                    <p className={styles.contactHeaderBox}>KONTAKTPERSON</p>
                    }
                    <div className={styles.infoElement}>
                        <span className="material-icons">person</span>
                        <p>{contact.fullName}</p>
                    </div>
                    <div className={styles.infoElement}>
                        <span className="material-icons">phone</span>
                        <p>{contact.phone}</p>
                        </div>
                    <div className={styles.infoElement}>
                        <span className="material-icons">mail_outline</span>
                        <p>{contact.email}</p></div>
                    <div className={styles.infoElement}>
                        <span className="material-icons">home</span>
                        <p>{contact.address}</p></div>
                    <div className={styles.infoElement}>
                        <span className="material-icons">fingerprint</span>
                        <p>Personnr: {contact.personNumber}</p>
                    </div>

                </div>
                }{!contact &&
            <LoaderComponent/>
            }
            </CardContent>
        </Card>
    )
}

export default ContactsDisplay
