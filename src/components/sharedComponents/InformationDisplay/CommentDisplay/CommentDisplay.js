import styles from "./CommentDisplay.module.css"

function CommentDisplay({message, visible}) {

    return (
        <div>

            {visible &&
            <div className={styles.publicCommentContainer}>
                <h3 style={{color: 'white'}}>Kommentar til selskapet</h3>
                <p>{message}</p>
            </div>}

            {!visible &&
            <div className={styles.privateCommentContainer}>
                <h3 style={{color: 'white'}}>Intern kommentar</h3>
                <p>{message}</p>
            </div>}


        </div>
    )
}

export default CommentDisplay



/*
<h5>Comment</h5>
            <div>

                {visible &&
                <p className="visible-comment">{message}</p>
                }
                {!visible &&
                <p className="invisible-comment">{message}</p>
                }
            </div>
 */