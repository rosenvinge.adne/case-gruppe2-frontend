import {useEffect, useState} from "react";
import {getFromEndpoint} from "../../../util/apiRequests/apiRequests";
import ContactsDisplay from "./ContactsDisplay/ContactsDisplay";
import AllApplicationDisplay from "./AllApplicationsDisplay/AllApplicationsDisplay";
import styles from "./ContactsDisplay/ContactDisplay.module.css"
import styles2 from "./CompanyInformationDisplay.module.css"


function CompanyInformationDisplay({companyEndpoint, displayPrivateComments = false}) {

    const [company, setCompany] = useState(null)
    const [error, setError] = useState ({message:'', status:0})

    useEffect(() => {
        if (company === null) {
            getFromEndpoint(companyEndpoint)
                .then(c => {
                    if (c.status === 400){
                        setError({message: 'This company does not exist in out database', status: c.status})
                    }else{
                        setCompany(c)
                        setError(null)
                    }
                })

        }
    }, [companyEndpoint])

    return (
        <div>
            <div>
                {error === null &&
                <div className={styles2.companyInfoContainer}>
                    <br/>
                    <h3>Navn: <strong>{company.name}</strong></h3>
                    <h3>Organisasjonsnummer: <strong>{company.id}</strong></h3>
                    {company.bankrupt &&
                    <h3>Sekslapet er konkurs</h3>
                    }
                    {company.underLiquidation &&
                    <h3>Selskapet er under avvikling</h3>
                    }
                    {company.contacts &&
                    <div className={styles.contactsContainer}>
                        {company.contacts.map((value, index) => {
                            return <ContactsDisplay contactsEndpoint={value} key={index}/>
                        })
                        }
                    </div>
                    }


                    {company.applications.slice(0).reverse().map((value, index) => {
                        return <AllApplicationDisplay applicationEndpoint={value} key={index}
                                                      displayPrivateComments={displayPrivateComments}/>
                    })
                    }
                </div>
                }
                {error !== null &&
                <p>{error.message}</p>
                }
            </div>
        </div>
    )
}

export default CompanyInformationDisplay
