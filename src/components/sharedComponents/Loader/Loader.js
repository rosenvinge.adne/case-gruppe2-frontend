import Loader from "react-loader-spinner";


function LoaderComponent () {

    // TODO: pop opp en bedskjed når loadern har stått i over 15 sec

    return (
        <div>
            <Loader type="ThreeDots" color="#999999" height={80} width={80} />
        </div>
    )
}

export default LoaderComponent