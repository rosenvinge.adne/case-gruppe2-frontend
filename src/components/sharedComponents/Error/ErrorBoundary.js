import React, { Component } from 'react';
import {Link} from "react-router-dom";
import ROUTE_PATH from "../../../util/paths/RoutePath";

export default class ErrorBoundary extends Component {

    state = { hasError: false };

    static getDerivedStateFromError(error) {
        // Update state to show the fallback UI during the next render phase
        return { hasError: true };
    }

    componentDidCatch(error, info) {
        // logging the error details
        console.log(`Cause: ${error}.\nStackTrace: ${info.componentStack}`);
    }

    render() {
        if (this.state.hasError) {
            // Return the fallback UI
            return <h3 style={{ 'text-align': 'center' }}>Unfortunately, something went wrong. Click <Link to={ROUTE_PATH.LANDING}>here</Link> to go</h3>;
        }

        return this.props.children;
    }
}