import {Card, CardContent} from "@material-ui/core";
import {Link} from "react-router-dom";
import ROUTE_PATH from "../../../util/paths/RoutePath";

function MissingContent(){

    return (
        <Card>
            <CardContent>
                <h3 style={{ 'text-align': 'center' }}>404 - Page not found :(</h3>
                <p style={{ 'text-align': 'center' }}><Link to={ROUTE_PATH.LANDING}>Go back</Link></p>
            </CardContent>
        </Card>
    )
}

export default MissingContent