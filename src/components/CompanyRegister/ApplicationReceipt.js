import ROUTE_PATH from "../../util/paths/RoutePath";
import {Link} from "react-router-dom";


function ApplicationReceipt() {

    return (
        <div>
            <h2>Thank you for your application!</h2>
            <p>Case number has been sent to your email.</p>
            <p>Log in using organization number and case number 😊</p>
            <Link to={ROUTE_PATH.COMPANY_LOGIN}>
                <button type="button">Login</button>
            </Link>
        </div>
    )
}

export default ApplicationReceipt