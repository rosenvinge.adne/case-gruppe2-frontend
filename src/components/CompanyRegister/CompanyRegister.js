import CompanyRegisterForm from "./CompanyRegisterForm";
import styles from "../CompanyProfile/Navbar.module.css";
import ROUTE_PATH from "../../util/paths/RoutePath";
import {Link} from "react-router-dom";

function CompanyRegister() {

    return (
        <div>
            <nav>
                <form className="form-inline">
                    <Link to={ROUTE_PATH.LANDING}>
                        <button type="button"><h3>Aprila</h3></button>
                    </Link>
                </form>
            </nav>

            <CompanyRegisterForm/>
        </div>
    )
}

export default CompanyRegister
