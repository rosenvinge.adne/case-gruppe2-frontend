import { useState } from "react";
import {useDispatch, useSelector} from "react-redux";
import {companyInfoContactRegisterAction} from "../../store/actions/companyInfoActions";
import {validateInput} from "../CompanyLogin/InputValidation";

/**
 * TODO
 -	Apply button, when clicked -> IF org number already exist in db -> POST application to db for that company (and existing case number), ELSE -> POST company details to company table in db (info = org number, generated case number + info from enhetsregisteret api)
 -	Email with case number to company contacts
 -  Error handling
 -  Add functionality for adding several contacts
 *
 **/

function CompanyContactRegisterForm(props) {

    const [inputError, setInputError] = useState({})

    // send input values to state in parent (CompanyRegisterForm)
    function onContactInputChange(event) {
        props.onChange(event.target.name, event.target.value)
    }

    return (
        <div>
            {props.primary && <h2>Primary contact details</h2>}
            {!props.primary && <h2>Contact details</h2>}
            <form>
                <div>
                    <label htmlFor="fullName">Full Name: </label>
                    <input id="fullName" type="text" onChange={onContactInputChange} required/>
                    <small>{inputError.fullName}</small>
                </div>

                <div>
                    <label htmlFor="personNumber">Person Number: </label>
                    <input id="personNumber" type="text" onChange={onContactInputChange} required/>
                    <small>{inputError.personNumber}</small>
                </div>

                <div>
                    <label htmlFor="address">Address: </label>
                    <input id="address" type="text" onChange={onContactInputChange} required/>
                </div>

                <div>
                    <label htmlFor="email">Email: </label>
                    <input id="email" type="text" onChange={onContactInputChange} required/>
                    <small>{inputError.email}</small>
                </div>

                <div>
                    <label htmlFor="phone">Phone: </label>
                    <input id="phone" type="text" onChange={onContactInputChange} required/>
                    <small>{inputError.phone}</small>
                </div>

            </form>
        </div>
    )
}

export default CompanyContactRegisterForm
