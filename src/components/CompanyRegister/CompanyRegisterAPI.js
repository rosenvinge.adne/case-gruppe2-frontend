import {storageGet} from "../../util/storage/storage";
import {STORAGE_KEYS} from "../../util/storage/storageKeys";
import {API_PATHS} from "../../util/apiPaths/apiPaths";

// const {bankrupt, underLiquidation, companyName, address} = getCompanyInfo
export function getCompanyInfoFromBRREG(id) {
    return fetch(`https://data.brreg.no/enhetsregisteret/api/enheter/${id}`)
        .then(response => {
            if (response.status === 404){
                throw new Error(`Organisasjonen ${id} er ikke registrert i Brønnøysundregistrene`)
            }
            return response
        })
        .then(response => response.json())
        .then(companyInfo => {
            return {
                ...companyInfo, // response from api, the rest is manually added
                id, // is overwritten if not included here
                companyName: companyInfo.navn,
                address: companyInfo.forretningsadresse.adresse.join(', ') + ' ' + companyInfo.forretningsadresse.postnummer + ' ' + companyInfo.forretningsadresse.poststed,
                bankrupt: companyInfo.konkurs,
                underLiquidation: companyInfo.underAvvikling
            }
        })
}

// Post request to add company to db
export function registerCompany(companyInfo) {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            id: Number(companyInfo.id),
            name: companyInfo.companyName,
            caseNr: companyInfo.caseNumber,
            address: companyInfo.address,
            bankrupt: companyInfo.bankrupt,
            underLiquidation: companyInfo.underLiquidation
        })
    };

    return fetch(API_PATHS.BASE+'/api/v1/company', requestOptions)
        .then(response => response.json()) //returns response as an object
}

// Post request to add application to db
export function registerApplication(companyInfo) {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            bankrupt: companyInfo.bankrupt,
            underLiquidation: companyInfo.underLiquidation
        })
    };

    return fetch(`${API_PATHS.BASE}/api/v1/company/${companyInfo.id}/application`, requestOptions)
        .then(response => response.json())
}

// Post request to add contact to db
export function registerContact(contactInfo){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            fullName: contactInfo.fullName,
            address: contactInfo.address,
            phone: contactInfo.phone,
            email: contactInfo.email,
            personNumber: contactInfo.personNumber,
            primaryContact: contactInfo.primaryContact,
            company: contactInfo.company
        })
    };
    return fetch(API_PATHS.BASE+'/api/v1/contact', requestOptions)
        .then(response => {
            if (response.status === 409) {  // conflict error
                throw new Error('E-postadressen er allerede regisrert')
            }
            return response
            }
        )
        .then(response => response.json())
}

// post user
export function registerUser(username){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            username: username
        })
    };
    return fetch(API_PATHS.BASE+'/api/auth/signup-customer', requestOptions)
        .then(response => response.json())
        //.then(response => response.data)
        .catch(e => {
            console.log(e.message)
        })
}

export function companySignIn(username, password){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            username: username,
            password: password
        })
    };
    return fetch(API_PATHS.BASE+'/api/auth/signin', requestOptions)
        .then(response => response.json())
        .catch(e => e.message)
}

/*
 return fetch(API_PATHS.BASE+'/api/auth/signin', requestOptions)
        .then(response => {
            if (response.status === 401) {  // unauthorized
                throw new Error('Wrong organization number or password')
            }
            if (response.status === 400) {  // unauthorized
                throw new Error('sperret')
            }
            console.log(response)
            return response.json()
        })
        .catch(e => e.message)
}

const onLoginClick = async () => {
        const response = await companySignIn(companyNr, caseNr)
        if(response instanceof Error){
            setError(response)
        }else{
            setLoginResponse(response)
        }
    }
 */

function getAccessToken() {
    const user = storageGet(STORAGE_KEYS.COMPANY)
    return user.accessToken
}

// Get company from db
export function getCompany(id) {
    const requestOptions = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            Authorization: `Bearer ${getAccessToken()}`
        }
    };
    return fetch(`${API_PATHS.BASE}/api/v1/company/${id}`, requestOptions)
        .then(response => {
            if (response.status === 404){
                throw new Error(`Organisasjonsummer ${id} eksisterer ikke`)
            }
            return response
        })
        .then(response => response.json())
}


// Check if company id and case number fetched from db match info from input
export function companyLogin(id, caseNr) {
    return getCompany(id)
        .then((company) => {
            if(company.caseNr !== caseNr){
                throw new Error('Feil brukernavn eller passord')
            }
            return company;

        })
}

// Check if company is registered in db
export function companyExists(id) {
    return fetch(`${API_PATHS.BASE}/api/v1/company/exists/${id}`)
        .then(response => response.json())
}