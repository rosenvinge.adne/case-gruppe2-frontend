import {useDispatch, useSelector} from "react-redux";
import ROUTE_PATH from "../../util/paths/RoutePath";
import {useState} from "react";
import {Link} from "react-router-dom";
import {
    companyInfoApplicationRegisterAction,
    companyInfoContactRegisterAction,
    companyInfoRegisterAction,
    companyInfoSetAction,
    companyInfoSetFetchingAction,
} from "../../store/actions/companyInfoActions";
import {useHistory} from "react-router";
import {companyExists, registerUser} from "./CompanyRegisterAPI";
import {useForm} from "react-hook-form";
import {PATTERN} from "../../util/inputValidation/patterns";
import styles from "./CompanyRegister.module.css"
import {Alert} from "@material-ui/lab";

function CompanyRegisterForm() {

    const [getInfoClicked, setGetInfoClicked] = useState(false) // må binde denne opp til valid input
    const {companyInfo, fetching, error} = useSelector(state => state.companyInfoReducer)
    const dispatch = useDispatch()
    const history = useHistory()
    const [err, setErr] = useState('')
    const [userRegistrationResponse, setUserRegistrationResponse] = useState({})
    const {register, handleSubmit, errors} = useForm();
    const [errorMsg, setErrorMsg] = useState('');

    const [primaryContact, setPrimaryContact] = useState([{
        fullName: '',
        personNumber: '',
        address: '',
        email: '',
        phone: ''
    }])

    // Save input value to contactInfo keys from child
    const onPrimaryContactInputChange = event => {
        setPrimaryContact({
            ...primaryContact,
            [event.target.name]: event.target.value
        })
    }

    // Save input value to companyInfo keys
    const onCompanyNumberInputChange = event => {
        dispatch(companyInfoSetAction({
            ...companyInfo,
            [event.target.name]: event.target.value
        }))
    }

    // Get company info from Enhetsregisteret
    async function onGetInfoFromBRREGClick() {
        const exists = await companyExists(companyInfo.id)
        if (!exists) {
            setGetInfoClicked(true)
            setErr('')
            await dispatch(companyInfoSetFetchingAction(companyInfo.id))
        } else {
            setErr(`Organisasjonsnummeret ${companyInfo.id} er allerede registrert. Vennligst logg inn.`)
            setGetInfoClicked(false)
        }
    }

    // Post company
    async function companyRegister(caseNumber) {
        dispatch(companyInfoRegisterAction({...companyInfo, caseNumber}))
    }

    // Post application
    async function applicationRegister() {
        dispatch(companyInfoApplicationRegisterAction(companyInfo))
    }

    // Post contact
    async function contactRegister(contact) {
        dispatch(companyInfoContactRegisterAction(contact))
    }

    async function onApplyClick() {
        const {caseNumber, message, secretImageUri} = await registerUser(companyInfo.id)

        await companyRegister(caseNumber)
        await applicationRegister()

        const primary = {
            ...primaryContact,
            primaryContact: true,
            company: {id: companyInfo.id}
        }

        try {
            await contactRegister(primary)
        } catch (e) {
            setErrorMsg(e.message)
        }

        setUserRegistrationResponse({message, caseNumber, secretImageUri});

        //history.push(ROUTE_PATH.COMPANY_APPLICATION_RECEIPT)
    }

    return (
        <div className={styles.registrationForm}>
            <div className={styles.blueSection}></div>
            <div className={styles.formContainer}>
                {!userRegistrationResponse.secretImageUri &&
                <div>
                    <h2>REGISTRER SELSKAP</h2>
                    <div className={styles.inputFieldContainer}>
                        <form onSubmit={handleSubmit(onGetInfoFromBRREGClick)}>
                            <input className={styles.inputField} placeholder="Organisasjonsnummer" name="id"
                                   onChange={onCompanyNumberInputChange} ref={register({
                                required: true,
                                minLength: 9,
                                maxLength: 9,
                                pattern: PATTERN.NUMBER
                            })}/>
                            <input className={styles.button} type="submit" disabled={fetching} value="NESTE"/>
                            {!errors.id && <p style={{height: '1em'}}></p>}
                            {errors.id && errors.id.type === 'required' &&
                            <p style={{height: '1em', color: "darkred"}}>Obligatorisk felt</p>}

                            {errors.id && errors.id.type === 'pattern' &&
                            <p style={{height: '1em', color: "darkred"}}>Kun tall er gyldig</p>}

                            {errors.id && (errors.id.type === 'minLength' || errors.id.type === 'maxLength') &&
                            <p style={{height: '1em', color: "darkred"}}>Organisasjonsnummer består av 9 siffer</p>}

                        </form>
                    </div>

                    {error && <Alert severity="error">{error}</Alert>}
                    {fetching && <p>Getting company info...</p>}

                    {err &&
                    <div>
                        <Alert severity="error">{err}</Alert>
                        <br/>
                        <Link to={ROUTE_PATH.COMPANY_LOGIN}>
                            <button className={styles.button} type="button">LOGG INN</button>
                        </Link>
                    </div>}

                    {getInfoClicked && error === null &&
                    <div className={styles.formFields}>
                        <div className={styles.companyInfoContainer}>
                            <p>Navn: {companyInfo.companyName}</p>
                            <p>Adresse: {companyInfo.address}</p>
                        </div>

                        {/* PRIMARY CONTACT REGISTER FORM */}
                        <form onSubmit={handleSubmit(onApplyClick)}>
                            <h2 style={{fontSize: '1.2em', marginTop: '1.2em'}}>HOVEDKONTAKT</h2>
                            <input className={styles.inputField} placeholder="Fullt navn" name="fullName" onChange={onPrimaryContactInputChange}
                                   ref={register({required: true, pattern: PATTERN.NAME})}/>
                            {errors.fullName && errors.fullName.type === 'required' && <p className={styles.inputError}>Obligatorisk felt</p>}
                            {errors.fullName && errors.fullName.type === 'pattern' && <p className={styles.inputError}>Kun bokstaver</p>}
                            {!errors.fullName && <p style={{height: '1em'}}></p>}


                            <input className={styles.inputField} placeholder="Personnummer" name="personNumber" onChange={onPrimaryContactInputChange} ref={register({
                                required: true,
                                minLength: 11,
                                maxLength: 11,
                                pattern: PATTERN.NUMBER
                            })}/>
                            {errors.personNumber && errors.personNumber.type === 'required' && <p className={styles.inputError}>Obligatorisk felt</p>}
                            {errors.personNumber && errors.personNumber.type === 'pattern' && <p className={styles.inputError}>Kun tall</p>}
                            {errors.personNumber && (errors.personNumber.type === 'minLength' || errors.personNumber.type === 'maxLength') &&
                            <p className={styles.inputError}>Personnummer består av 11 siffer</p>}
                            {!errors.personNumber && <p style={{height: '1em'}}></p>}


                            <input className={styles.inputField} placeholder="Adresse" name="address" onChange={onPrimaryContactInputChange}
                                   ref={register({required: true, pattern: PATTERN.ADDRESS})}/>
                            {errors.address && errors.address.type === 'required' && <p className={styles.inputError}>Obligatorisk felt</p>}
                            {errors.address && errors.address.type === 'pattern' && <p className={styles.inputError}>Ugyldige tegn</p>}
                            {!errors.address && <p style={{height: '1em'}}></p>}

                            <input className={styles.inputField} placeholder="Telefon" name="phone" onChange={onPrimaryContactInputChange}
                                   ref={register({required: true, pattern: PATTERN.NUMBER})}/>
                            {errors.phone && errors.phone.type === 'required' && <p className={styles.inputError}>Obligatorisk felt</p>}
                            {errors.phone && errors.phone.type === 'pattern' && <p className={styles.inputError}>Kun tall</p>}
                            {!errors.phone && <p style={{height: '1em'}}></p>}

                            <input className={styles.inputField} placeholder="E-post" name="email" onChange={onPrimaryContactInputChange}
                                   ref={register({required: true, pattern: PATTERN.EMAIL})}/>
                            {errors.email && errors.email.type === 'required' && <p className={styles.inputError}>Obligatorisk felt</p>}
                            {errors.email && errors.email.type === 'pattern' && <p className={styles.inputError}>Ugyldig e-postadresse</p>}
                            {!errors.email && <p style={{height: '1em'}}></p>}

                            <input className={styles.button} type="submit" onChange={onApplyClick} value="REGISTRER" ref={register}/>
                        </form>

                        {errorMsg && <Alert severity="error">{errorMsg}</Alert>}
                    </div>}
                </div>}

                {userRegistrationResponse.secretImageUri &&
                <div>
                    {userRegistrationResponse.message && <h2>Konto registrert</h2>}
                    <p>Skan QR-koden for å registrere tofaktorautentisering</p>
                    <img src={userRegistrationResponse.secretImageUri} alt={"two step authentication QR code"}/>
                    <div>
                        <Link to={ROUTE_PATH.COMPANY_LOGIN}>
                            <button className={styles.button} type="button">LOGG INN</button>
                        </Link>
                    </div>
                </div>
                }

            </div>
        </div>
    )
}

export default CompanyRegisterForm