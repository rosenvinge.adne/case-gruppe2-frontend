
export const API_PATHS = {
    BASE: 'https://aprila-service.herokuapp.com',
    GET_CUSTOMER_PATH: '/api/v1/company',
    ENHETS_REG_BASE: 'https://data.brreg.no/enhetsregisteret/api'
}
