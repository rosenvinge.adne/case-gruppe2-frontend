const ROUTE_PATH = {
    LANDING: '/',
    COMPANY_LOGIN: '/company-login',
    COMPANY_PROFILE: '/company-profile',
    COMPANY_REGISTRATION: '/company-registration',
    COMPANY_APPLICATION_RECEIPT: '/application',

    COMPANY_ADD_CONTACT: '/new-contact',
    COMPANY_ADD_APPLICATION: '/new-application',

    TWO_FACTOR: 'verification',
    //  CaseHandler paths:
    CH_LOGIN: '/case-handler/login',
    CH_REGISTER: '/case-handler/register',


    CH_HOME: '/case-handler/home',
    CH_CUSTOMER_PROFILE: `/case-handler/customer`,
    CH_REVIEW_APPLICATION: `/case-handler/review`,
    CASE_HANDLER: '/case-handler'

}
export default ROUTE_PATH
