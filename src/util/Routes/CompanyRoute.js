import { Route, Redirect } from "react-router-dom";
import {storageGet} from "../storage/storage";
import {STORAGE_KEYS} from "../storage/storageKeys";
import ROUTE_PATH from "../paths/RoutePath";

const CompanyRoute = props => {

    const user = storageGet(STORAGE_KEYS.COMPANY)

    if(user && user.roles.includes('ROLE_CUSTOMER')){
        return <Route {...props}/>
    }else{
        return <Redirect to={ROUTE_PATH.LANDING}/>
    }
}

export default CompanyRoute