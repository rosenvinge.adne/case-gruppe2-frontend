export const PATTERN = {
    NAME: /^[a-zA-ZÆæØøÅå\-.,\s]+$/,
    ADDRESS: /^[a-zA-Z0-9ÆæØøÅå\-.,\s]+$/,
    NUMBER: /^\d*$/,
    EMAIL: /\S+@\S+\.\S+/
}