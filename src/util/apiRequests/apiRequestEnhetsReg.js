import {API_PATHS} from "../apiPaths/apiPaths";


/**
 * Fetch data from the Enhetsregister
 * @param: orgNumber -> organization Number
 * */
export const getCompanyInfoFromEnhetsReg = async (orgNumber) => {
    console.log(`fetching data from enhetsregisteret, orgnumber: ${orgNumber} ...`)

    return fetch(`${API_PATHS.ENHETS_REG_BASE}/enheter/${orgNumber}`)
        .then(response => response.json())
}