import {API_PATHS} from "../apiPaths/apiPaths";

import {storageGet} from "../storage/storage";
import {STORAGE_KEYS} from "../storage/storageKeys";

// Fetch access token from local storage
function updateAuthorizationKey(){
    const user = storageGet(STORAGE_KEYS.USER)
    return user.accessToken
}

/*   GENERIC FETCH METHOD    */
/**
 * GET- when the database refers to another endpoint
 * @param: endpoint: api endpoint
 * get an endpoint, ie. for contact, application, comment, casehandler or company
 * */
export const getFromEndpoint = async (endpoint) => {
    const getHeader = {
        method: 'GET',
        headers:{
            Accept: 'application/json',
            Authorization: `Bearer ${updateAuthorizationKey()}`
        }
    }
    console.log(`Fetching: ${API_PATHS.BASE}${endpoint} ...`)

    return fetch(`${API_PATHS.BASE}${endpoint}`, getHeader)
        .then(response => response.json())
}

/**
 * GET- all application info, including all comments, from the application endpoint
 * @param: applicationId -> /api/v1/application/{application_id}
 * */
export const getAllApplicationInfo = async (applicationId) => {

    const getHeader = {
        method: 'GET',
        headers:{
            Accept: 'application/json',
            Authorization: `Bearer ${updateAuthorizationKey()}`
        }
    }

    console.log(`Fetching all application (${applicationId}) info...`)

    let application = await fetch(`${API_PATHS.BASE}/api/v1/application/${applicationId}`, getHeader)
        .then(response => response.json())
        .catch(error => {
            console.log(error.message)
        })

    application.comments = await getAllCommentsInApplication(application.id)

    return application
}
// helper function for getAllApplicationInfo()
/**
 * returns all comment obj
 * */
export const getAllCommentsInApplication = async (application_id) => {
    const getHeader = {
        method: 'GET',
        headers:{
            Accept: 'application/json',
            Authorization: `Bearer ${updateAuthorizationKey()}`
        }
    }

    return fetch(`${API_PATHS.BASE}/api/v1/application/${application_id}/comment`, getHeader)
        .then(response => response.json())
        .catch(error => {
            console.log(error.message)
        })
}

/*   COMPANY API REQUESTS   */

/**
 * GET- request - all companies
 * */
export const getAllCompanies = async () => {
    const getHeader = {
        method: 'GET',
        headers:{
            Authorization: `Bearer ${updateAuthorizationKey()}`
        }
    }

    console.log('fetching all companies...')

    return fetch(`${API_PATHS.BASE}/api/v1/company/`, getHeader)
        .then(response => response.json())
        .catch(error => {
            console.log(error.message)
        })
}

/**
 * GET- request - single company
 * @param: orgNumber
 * */
export const getCompany = async (id) => {
    const getHeader = {
        method: 'GET',
        headers:{
            Authorization: `Bearer ${updateAuthorizationKey()}`
        }
    }


    console.log('fetching company: '+id+'...')

    return fetch(`${API_PATHS.BASE}/api/v1/company/${id}`, getHeader)
        .then(response => response.json())
        .catch(error => {
            console.log(error.message)
        })
}


/**
 * GET - all pending applications
 *
 * */
export const getAllPendingApplications = async () => {

    const getHeader = {
        method: 'GET',
        headers:{
            Authorization: `Bearer ${updateAuthorizationKey()}`
        }
    }

    console.log('fetching all pending application...')

    return fetch(`${API_PATHS.BASE}/api/v1/application/pending`, getHeader)
        .then(response => response.json())
        .catch(error => {
            console.log(error.message)
        })
}

/**
 * GET - Single application
 * @param: applicationId -> int
 * */
export const getSingleApplication = async (applicationId) => {
    const getHeader = {
        method: 'GET',
        headers:{
            Accept: 'application/json',
            Authorization: `Bearer ${updateAuthorizationKey()}`
        }
    }


    console.log('fetching application ' + applicationId + '...')

    return fetch(`${API_PATHS.BASE}/api/v1/application/${applicationId}`, getHeader)
        .then(response => response.json())
        .catch(error => {
            console.log(error.message)
        })
}

/**
 * POST - comment to application
 * @param: applicationId - int/string, message - string, visible - boolean
 * */
export const postCommentToApplication = (applicationId, message, visible) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'authorization': `Bearer ${updateAuthorizationKey()}`
        },
        body: JSON.stringify({
            message: message,
            visible: visible,
        })
    };
    return fetch(`${API_PATHS.BASE}/api/v1/application/${applicationId}/comment`, requestOptions)
        .then(response => response.json())
        .catch(e => {
            console.error(e.message)
        });
}

/**
 * PUT - Update application status -> approved/denied
 *
 *
 * */
export const putApplicationStatus = async (applicationId, applicationNewStatus) => {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'authorization': `Bearer ${updateAuthorizationKey()}`
        },
    };
    return fetch(`${API_PATHS.BASE}/api/v1/application/${applicationId}?NewStatus=${applicationNewStatus}`, requestOptions)
        .then(response => response.json())
        .catch(e =>{
            console.log(e.message)
        })
}

/*
        LOGIN AND REGISTER
 */
export const loginCaseHandler = async ({username, password}) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username: username,
            password: password,
        })
    };

    return await fetch(`${API_PATHS.BASE}/api/auth/signin`, requestOptions)
        .then(response => response.json())
        //.catch(error => error.message)
}

export const verifyTwoFactor = async (username, password, code) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username: username,
            password: password,
            code: code
        })
    };

    return await fetch(`${API_PATHS.BASE}/api/auth/verify`, requestOptions)
        .then(response => response.json())
    //.catch(error => error.message)
}

export const registerNewCaseHandler = ({username, password}) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username: username,
            password: password,
        })
    };

    return fetch(`${API_PATHS.BASE}/api/auth/signup-casehandler`, requestOptions)
        .then(response => response.json())

}
