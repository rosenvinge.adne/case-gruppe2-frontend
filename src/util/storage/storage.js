/** Local Storage
 * Setter, Getter and Remover for local storage
 * */
export const storageSet = (key, value) => {
    const json = JSON.stringify(value)
    const encoded = btoa( json )
    localStorage.setItem( key, encoded )
}

export const storageGet = key => {
    const stored = localStorage.getItem(key)
    if (!stored) {
        return false
    }

    const decoded = atob( stored )
    return JSON.parse( decoded )
}

export const storageRemove = key => {
    localStorage.removeItem(key)
}
