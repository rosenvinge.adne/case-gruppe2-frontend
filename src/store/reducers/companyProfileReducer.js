// state is specifically for this reducer, can only access the company info state


// only if state is undefined


import {ACTION_COMPANY_PROFILE_SET, ACTION_COMPANY_PROFILE_UPDATE} from "../actions/companyProfileActions";

const initialState = {
    //fetching: false,
    companyProfile: {
        username: '',
        accessToken: '',
        tokenType: '',
        roles: []
    }
}

// return new value/variable of state which can be fetched
export function companyProfileReducer(state = initialState, action) {
    switch(action.type){
        case ACTION_COMPANY_PROFILE_SET:
            return {
                companyProfile: action.payload, // response from http request, company info object
                //fetching: false
            }
        case ACTION_COMPANY_PROFILE_UPDATE:
            return {
                companyProfile: action.payload, // response from http request, company info object
                //fetching: false,
            }
        default:
            return state // for the first time the app runs
    }
}
