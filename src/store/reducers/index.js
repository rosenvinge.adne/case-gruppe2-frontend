import {combineReducers} from "redux";
import {companyInfoReducer} from "./companyInfoReducer";
import {companyProfileReducer} from "./companyProfileReducer";

export default combineReducers({
    companyInfoReducer,
    companyProfileReducer
})
