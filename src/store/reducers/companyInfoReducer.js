// state is specifically for this reducer, can only access the company info state


// only if state is undefined
import {
    ACTION_COMPANY_INFO_SET,
    ACTION_COMPANY_INFO_SET_ERROR,
    ACTION_COMPANY_INFO_SET_FETCHING
} from "../actions/companyInfoActions";

const initialState = {
    fetching: false,
    companyInfo: {
        id: '',
        companyName: '',
        caseNr: '',
        address: '',
        bankrupt: '',
        underLiquidation: ''
    },
    error: ''
}

// return new value/variable of state which can be fetched
export function companyInfoReducer(state = initialState, action) {
    switch(action.type){
        case ACTION_COMPANY_INFO_SET_FETCHING:
            return {
                ...state,
                fetching: true
            }
        case ACTION_COMPANY_INFO_SET:
            return {
                companyInfo: action.payload, // response from http request, company info object
                fetching: false,
                error: null
            }
        case ACTION_COMPANY_INFO_SET_ERROR:
            return {
                ...state, //save old company info
                fetching: false,
                error: action.payload //the error message
            }
        default:
            return state // for the first time the app runs
    }
}
