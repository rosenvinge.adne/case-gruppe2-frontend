import { applyMiddleware, createStore } from 'redux';
import rootReducer from './reducers';
import {composeWithDevTools} from "redux-devtools-extension";
import {companyInfoMiddleware} from "./middleware/companyInfoMiddleware";
import {companyProfileMiddleware} from "./middleware/companyProfileMiddleware";

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(
        companyInfoMiddleware,
        companyProfileMiddleware
        // new middleware here
    ))
);

export default store;
