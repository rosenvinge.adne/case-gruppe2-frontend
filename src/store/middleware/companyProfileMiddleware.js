import {storageSet} from "../../util/storage/storage";
import {STORAGE_KEYS} from "../../util/storage/storageKeys";
import {ACTION_COMPANY_PROFILE_SET, ACTION_COMPANY_PROFILE_UPDATE} from "../actions/companyProfileActions";

export const companyProfileMiddleware = ({ getState, dispatch }) => next => async action => {
    next(action)

    if (action.type === ACTION_COMPANY_PROFILE_SET || action.type === ACTION_COMPANY_PROFILE_UPDATE) {
        storageSet(STORAGE_KEYS.COMPANY, action.payload)
    }
}
