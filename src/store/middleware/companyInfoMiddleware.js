import {
    ACTION_COMPANY_INFO_APPLICATION_REGISTER, ACTION_COMPANY_INFO_CONTACT_REGISTER,
    ACTION_COMPANY_INFO_REGISTER,
    ACTION_COMPANY_INFO_SET_FETCHING, ACTION_COMPANY_INFO_USER_REGISTER,
    companyInfoSetAction,
    companyInfoSetErrorAction
} from "../actions/companyInfoActions";
import {
    getCompanyInfoFromBRREG,
    registerApplication,
    registerCompany, registerContact, registerUser
} from "../../components/CompanyRegister/CompanyRegisterAPI";

export const companyInfoMiddleware = ({ getState, dispatch }) => next => async action => {
    next(action)

    if(action.type === ACTION_COMPANY_INFO_SET_FETCHING) {
        try{
            const company = await getCompanyInfoFromBRREG(action.payload)
            dispatch(companyInfoSetAction(company))
        }catch(e){
            dispatch(companyInfoSetErrorAction(e.message));
        }
    }

    // will only happen if we click register
    if(action.type === ACTION_COMPANY_INFO_REGISTER) {
        try{
            await registerCompany(action.payload)
        }catch(e){
            dispatch(companyInfoSetErrorAction(e.message));
        }
    }

    // for application register
    if(action.type === ACTION_COMPANY_INFO_APPLICATION_REGISTER) {
        try{
            await registerApplication(action.payload)
        }catch(e){
            dispatch(companyInfoSetErrorAction(e.message));
        }
    }

    // register customer
    if(action.type === ACTION_COMPANY_INFO_CONTACT_REGISTER) {
        try{
            await registerContact(action.payload)
        }catch(e){
            dispatch(companyInfoSetErrorAction(e.message))
        }
    }

    if(action.type === ACTION_COMPANY_INFO_USER_REGISTER) {
        try{
            await registerUser(action.payload)
        }
        catch(e){
            dispatch(companyInfoSetErrorAction(e.message))
        }
    }
}
