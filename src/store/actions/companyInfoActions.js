export const ACTION_COMPANY_INFO_SET_FETCHING = '[companyInfo] SET_FETCHING'
export const ACTION_COMPANY_INFO_SET = '[companyInfo] SET'
export const ACTION_COMPANY_INFO_SET_ERROR = '[companyInfo] SET_ERROR'
export const ACTION_COMPANY_INFO_REGISTER = '[companyInfo] REGISTER'
export const ACTION_COMPANY_INFO_APPLICATION_REGISTER = '[companyInfo] APPLICATION REGISTER'
export const ACTION_COMPANY_INFO_CONTACT_REGISTER = '[companyInfo] CONTACT REGISTER'
export const ACTION_COMPANY_INFO_USER_REGISTER = '[companyInfo] USER REGISTER'


export const companyInfoSetFetchingAction = payload => ({
    type: ACTION_COMPANY_INFO_SET_FETCHING,
    payload // company id
})

export const companyInfoSetAction = payload => ({
    type: ACTION_COMPANY_INFO_SET,
    payload // company data
})

export const companyInfoSetErrorAction = payload => ({
    type: ACTION_COMPANY_INFO_SET_ERROR,
    payload // error message
})

// for visual feedback in redux to see what changed
export const companyInfoRegisterAction = payload => ({
    type: ACTION_COMPANY_INFO_REGISTER,
    payload
})

// for visual feedback in redux to see that application registration happened
export const companyInfoApplicationRegisterAction = payload => ({
    type: ACTION_COMPANY_INFO_APPLICATION_REGISTER,
    payload
})

// for visual feedback in redux to see that contact registration happened
export const companyInfoContactRegisterAction = payload => ({
    type: ACTION_COMPANY_INFO_CONTACT_REGISTER,
    payload
})

// for visual feedback in redux to see that user registration happened
export const companyInfoUserRegisterAction = payload => ({
    type: ACTION_COMPANY_INFO_USER_REGISTER,
    payload
})

