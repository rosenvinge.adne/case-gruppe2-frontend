export const ACTION_COMPANY_PROFILE_SET = '[companyProfile] SET';
export const ACTION_COMPANY_PROFILE_UPDATE = '[companyProfile] UPDATE';

export const companyProfileSetAction = payload => ({
    type: ACTION_COMPANY_PROFILE_SET,
    payload // company profile
})

export const companyProfileUpdateAction = payload => ({
    type: ACTION_COMPANY_PROFILE_UPDATE,
    payload // company profile
})
