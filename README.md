# Aprila Customer Application Service - ACAS
### Front End
#### Amalie, Edris, Sondre and Ådne

### About

This is our final task in Norof's accelerated learning program. We worked in a group 
of four developers, two back end and two front end to answer a case we recived from noroff

The backend for this is located in a separate git repository: https://gitlab.com/edrisaf/aprila-spring

The complete app is deployed at https://aprila-bank.herokuapp.com/

USER GUIDE: https://drive.google.com/file/d/19RuABDi9F6ruN96Y_13mPeRzrECrYXJ-/view?usp=sharing


### Install guide local

1. Download the program for the back end part of the program and follow the guide for this installation first.

2. Download this project 

3. Locate the project file and open the folder in a terminal.

4. Run 'npm install'
- By default, 'npm install' will install all modules listed as dependencies in package.json. This is all the modules you need to run the application

5. Run 'react-scripts start'. This will launch the application on localhost:3000 by default

6. Open a browser and navigate to http://localhost:3000/

Note: When running the backend localy you need no navitage to 'src/util/apiPaths/apiPaths.js' and change line 3:
```
BASE: 'https://aprila-service.herokuapp.com',
```

to 

```
BASE: 'https://localhost:8080',
```

Then you should be all good!
